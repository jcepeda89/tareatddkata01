from __future__ import division


class ProcesadorNumeros:
    def procesar(self, cadena):
        tamanio = 0
        minimo = None
        maximo = None
        promedio = None

        if len(cadena) == 1:
            tamanio = len(cadena)
            minimo = int(cadena)
            maximo = int(cadena)
            promedio = int(cadena)
        elif len(cadena) > 1 and "," in cadena:
            numeros = cadena.split(",")
            tamanio = len(numeros)
            minimo = int(min(numeros))
            maximo = int(max(numeros))
            suma = 0

            for numero in numeros:
                if numero != "":
                    suma += int(numero)

            promedio = suma / tamanio

        resultado = [tamanio, minimo, maximo, promedio]
        return resultado
