from unittest import TestCase

from ProcesadorNumeros import ProcesadorNumeros


class ProcesadorNumerosTest(TestCase):
    def test_procesar_cadena_vacia(self):
        # Iteracion 1: Devolver un arreglo con el numero de elementos
        resultado_iteracion_1 = [0]
        # Iteracion 2: Devolver un arreglo con el numero de elementos y el minimo
        resultado_iteracion_2 = [0, None]
        # Iteracion 3: Devolver un arreglo con el numero de elementos, el minimo y el maximo
        resultado_iteracion_3 = [0, None, None]
        # Iteracion 4: Devolver un arreglo con el numero de elementos, el minimo, el maximo y el promedio
        resultado_iteracion_4 = [0, None, None, None]

        self.assertEqual(ProcesadorNumeros().procesar(""), resultado_iteracion_4, "Cadena vacia")

    def test_procesar_cadena_con_numero(self):
        # Iteracion 1: Devolver un arreglo con el numero de elementos
        resultado_iteracion_1 = [1]
        # Iteracion 2: Devolver un arreglo con el numero de elementos y el minimo
        resultado_iteracion_2 = [1, 3]
        # Iteracion 3: Devolver un arreglo con el numero de elementos, el minimo y el maximo
        resultado_iteracion_3 = [1, 3, 3]
        # Iteracion 4: Devolver un arreglo con el numero de elementos, el minimo, el maximo y el promedio
        resultado_iteracion_4 = [1, 3, 3, 3]

        self.assertEqual(ProcesadorNumeros().procesar("3"), resultado_iteracion_4, "Cadena con un numero")

    def test_procesar_cadena_con_dos_numeros(self):
        # Iteracion 1: Devolver un arreglo con el numero de elementos
        resultado_iteracion_1 = [2]
        # Iteracion 2: Devolver un arreglo con el numero de elementos y el minimo
        resultado_iteracion_2 = [2, 1]
        # Iteracion 3: Devolver un arreglo con el numero de elementos, el minimo y el maximo
        resultado_iteracion_3 = [2, 1, 2]
        # Iteracion 4: Devolver un arreglo con el numero de elementos, el minimo, el maximo y el promedio
        resultado_iteracion_4 = [2, 1, 2, 1.5]

        self.assertEqual(ProcesadorNumeros().procesar("1,2"), resultado_iteracion_4, "Cadena con dos numeros")

    def test_procesar_cadena_con_n_numeros(self):
        # Iteracion 1: Devolver un arreglo con el numero de elementos
        resultado_iteracion_1 = [6]
        # Iteracion 2: Devolver un arreglo con el numero de elementos y el minimo
        resultado_iteracion_2 = [6, 0]
        # Iteracion 3: Devolver un arreglo con el numero de elementos, el minimo y el maximo
        resultado_iteracion_3 = [6, 0, 5]
        # Iteracion 4: Devolver un arreglo con el numero de elementos, el minimo, el maximo y el promedio
        resultado_iteracion_4 = [6, 0, 5, 2.5]

        self.assertEqual(ProcesadorNumeros().procesar("2,4,5,3,1,0"), resultado_iteracion_4, "Cadena con n numeros")
